setUpForm();
function setUpForm() {
  let form = document.getElementById("form_domaine");
  form.addEventListener("submit", (e) => {
    e.preventDefault();
    sendForm(form);
  });
}
function getTheBoy() {
  let xhr;
  try {
    xhr = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xhr = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e2) {
      try {
        xhr = new XMLHttpRequest();
      } catch (e3) {
        xhr = false;
      }
    }
  }
  return xhr;
}

function sendForm(form) {
  let xhr = getTheBoy();
  //
  let formData = new FormData(form);

  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var retour = JSON.parse(xhr.responseText);
        if (retour.status == "error") {
        } else {
          console.log(retour);
        }
      } else {
        console.log(xhr.status);
      }
    }
  };
  xhr.addEventListener("error", function (event) {
    alert("Oups! Quelque chose s'est mal passé lors de la publication .");
  });
  xhr.open("POST", `/projet_test/insert_domain_form.do`, true);
  xhr.send(formData);
}
