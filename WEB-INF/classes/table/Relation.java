package table;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import connection.*;

import java.sql.*;

public class Relation {

    public Relation() {
    }

    public Vector<Object> getAttributsName() {
        Vector<Object> attributs = new Vector<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            attributs.add(fields[i].getName());
        }

        return attributs;
    }

    public Vector<Object> getAttributsType() {
        Vector<Object> attributs = new Vector<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            attributs.add(fields[i].getType().getSimpleName());
        }

        return attributs;
    }

    public Vector<Object> getAttributs() {
        Vector<Object> attributs = new Vector<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            attributs.add(fields[i]);
        }
        return attributs;
    }

    public Vector<Object> getAttributsValues() {
        Vector<Object> attributs = new Vector<>();
        Field[] fields = this.getClass().getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            try {
                fields[i].setAccessible(true);
                attributs.add(fields[i].get(this));
            } catch (IllegalArgumentException e) {
                System.out.println("illegalArg");
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                System.out.println("illegalAcces");
                e.printStackTrace();
            }
        }
        return attributs;
    }

    public String getTableName() {
        return this.getClass().getSimpleName();
    }

    public void createTable(Connection connection) throws SQLException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        if (connection == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = connection;
        }
        try {
            requete += " CREATE TABLE " + this.getTableName() + " ( ";
            Vector<Object> attribsName = this.getAttributsName();
            Vector<Object> attribsType = this.getAttributsType();
            for (int i = 0; i < attribsName.size(); i++) {
                String typeCol = transformTypeCol(attribsType.get(i).toString());
                System.out.println(typeCol);
                if (typeCol.equalsIgnoreCase("DATE")) {
                    requete += attribsName.get(i) + " " + transformTypeCol(attribsType.get(i));
                } else {
                    requete += attribsName.get(i) + " " + transformTypeCol(attribsType.get(i)) + "(18)";
                }
                if (i < attribsName.size() - 1) {
                    requete += ",";
                }
            }

            requete += " ) ";
            System.out.println(requete);
            Statement stmt = con.createStatement();
            stmt.execute(requete);
            if (secours) {
                con.commit();
            }
        } catch (SQLException e) {

            con.rollback();
            System.err.println(e);
        } finally {
            if (secours) {
                con.close();
            }
        }
    }

    public void insert(Connection connection) throws SQLException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        if (connection == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = connection;
        }
        try {
            Vector<Object> attributs = this.getAttributsValues();
            Vector<Object> attribsType = this.getAttributsType();
            Field[] fields = this.getClass().getDeclaredFields();
            requete += "INSERT INTO " + this.getTableName() + " VALUES (";
            for (int i = 0; i < attributs.size(); i++) {
                if (isAnArray(fields[i]) || attribsType.get(i).equals("Service")
                        || attribsType.get(i).equals("Candidat") || attribsType.get(i).equals("Specialite")
                        || attribsType.get(i).equals("Gain") || attribsType.get(i).equals("Finition")
                        || attribsType.get(i).equals("Format")) {
                    continue;
                }
                if (transformTypeCol(attribsType.get(i)).equals("NUMERIC")
                        || transformTypeCol(attribsType.get(i)).equals("DATE") && attributs.get(i) == null
                        || transformTypeCol(attribsType.get(i)).equals("VARCHAR") && attributs.get(i) == null) {
                    requete += attributs.get(i);
                } else {

                    requete += "'" + attributs.get(i) + "'";
                }
                if (i < attributs.size() - 1) {
                    requete += ",";
                }
            }
            requete += ")";
            System.out.println(requete);

            Statement stmt = con.createStatement();
            stmt.execute(requete);
            if (secours) {
                con.commit();
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (secours) {
                con.close();
            }
        }
    }

    public Vector<Object> select(Connection connection, boolean all)
            throws SQLException, IllegalArgumentException, IllegalAccessException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        Vector<Object> objets = new Vector<Object>();
        if (Allnull()) {
            all = true;
        }
        if (connection == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = connection;
        }
        try {

            if (all) {

                requete += "SELECT * FROM " + this.getTableName();

                Statement stmt = con.createStatement();
                ResultSet res = stmt.executeQuery(requete);
                System.out.println(requete);
                ResultSetMetaData resMeta = res.getMetaData();
                int columnCount = resMeta.getColumnCount();
                while (res.next()) {

                    Vector<Object> dataProv = new Vector<Object>();
                    String row = "";
                    for (int i = 1; i < columnCount + 1; i++) {
                        row += res.getString(i) + "--sep--";
                    }
                    String[] datas = row.split("--sep--");
                    for (int o = 0; o < datas.length; o++) {
                        dataProv.add(datas[o]);
                    }
                    Constructor[] cons = this.getClass().getDeclaredConstructors();
                    for (int u = 0; u < cons.length; u++) {
                        cons[u].setAccessible(true);
                        try {
                            objets.add(cons[u].newInstance(dataProv));
                        } catch (Exception e) {
                            // System.out.println(e);
                        }
                    }
                }
            } else {
                requete += "SELECT * FROM " + this.getTableName() + " WHERE ";
                Field[] attFields = this.getClass().getDeclaredFields();
                Vector<Object> attribsType = this.getAttributsType();

                int moreSpecif = 0;
                for (int i = 0; i < attFields.length; i++) {
                    attFields[i].setAccessible(true);
                    if (isAnArray(attFields[i]) || attribsType.get(i).equals("Service")
                            || attribsType.get(i).equals("Candidat") || attribsType.get(i).equals("Specialite")
                            || attribsType.get(i).equals("Gain") || attribsType.get(i).equals("Finition")
                            || attribsType.get(i).equals("Format")) {
                        continue;
                    }
                    if (notNull(attFields[i])) {
                        if (attFields[i].getType().getSimpleName().equals("int")
                                || attFields[i].getType().getSimpleName().equals("Integer")
                                || attFields[i].getType().getSimpleName().equals("Double")
                                || attFields[i].getType().getSimpleName().equals("double")) {
                            if (moreSpecif > 0) {
                                requete += " AND " + attFields[i].getName() + " = " + attFields[i].get(this) + " ";
                            } else {
                                requete += attFields[i].getName() + " = " + attFields[i].get(this) + " ";
                            }
                        } else {
                            if (moreSpecif > 0) {
                                // lIKE was there
                                requete += " AND " + attFields[i].getName() + " = '" + attFields[i].get(this)
                                        + "'";
                            } else {
                                // lIKE was there
                                requete += attFields[i].getName() + " = '" + attFields[i].get(this) + "'";
                            }
                        }
                        moreSpecif++;
                    }
                }
                Statement stmt = con.createStatement();
                System.out.println(requete);
                ResultSet res = stmt.executeQuery(requete);
                ResultSetMetaData resMeta = res.getMetaData();
                int columnCount = resMeta.getColumnCount();
                while (res.next()) {
                    Vector<Object> dataProv = new Vector<Object>();
                    String row = "";
                    for (int i = 1; i < columnCount + 1; i++) {
                        row += res.getString(i) + "--Sep--";
                    }
                    String[] datas = row.split("--Sep--");
                    for (int o = 0; o < datas.length; o++) {
                        dataProv.add(datas[o]);
                    }
                    Constructor[] cons = this.getClass().getDeclaredConstructors();
                    for (int u = 0; u < cons.length; u++) {
                        try {
                            objets.add(cons[u].newInstance(dataProv));
                        } catch (Exception e) {
                            // System.out.println(e);
                        }
                    }
                }
            }

        } catch (SQLException e) {
            System.err.println(e);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            if (secours) {
                con.close();
            }
        }
        return objets;
    }

    public void update(Connection co, String identifiant) throws SQLException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        String idObject = identifiant;
        if (co == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = co;
        }
        try {
            if (AllnullUpdate(identifiant)) {
                throw new Exception("Update invalide => aucune attribut a modifier ");
            }
            requete += "UPDATE " + this.getTableName() + " SET ";
            Field[] attr = this.getClass().getDeclaredFields();
            Vector<Object> attribsType = this.getAttributsType();
            Field[] fields = this.getClass().getDeclaredFields();

            int next = 0;
            for (int i = 0; i < attr.length; i++) {
                attr[i].setAccessible(true);
                if (isAnArray(fields[i]) || attribsType.get(i).equals("Service")
                        || attribsType.get(i).equals("Candidat") || attribsType.get(i).equals("Specialite")
                        || attribsType.get(i).equals("Gain") || attribsType.get(i).equals("Finition")
                        || attribsType.get(i).equals("Format")) {
                    continue;
                }
                if (notNull(attr[i]) && !attr[i].getName().equals(identifiant)) {
                    if (next == 0) {
                        if (transformTypeCol(attr[i].getType().getSimpleName()).equals("NUMERIC")
                                || transformTypeCol(attr[i].getType().getSimpleName()).equals("DATE")
                                        && attr[i].get(this) == null) {
                            requete += attr[i].getName() + " = " + attr[i].get(this) + " ";
                        } else {
                            requete += attr[i].getName() + " = '" + attr[i].get(this) + "' ";
                        }
                        next++;
                    } else {
                        if (transformTypeCol(attr[i].getType().getSimpleName()).equals("NUMERIC")
                                || transformTypeCol(attr[i].getType().getSimpleName()).equals("DATE")
                                        && attr[i].get(this) == null) {
                            requete += ", " + attr[i].getName() + " = " + attr[i].get(this) + " ";
                        } else {
                            requete += ", " + attr[i].getName() + " = '" + attr[i].get(this) + "' ";
                        }
                    }
                }
            }

            Field id = idUpdate(identifiant);
            id.setAccessible(true);
            if (transformTypeCol(id.getType().getSimpleName()).equals("NUMERIC")) {
                requete += " WHERE " + identifiant + "= " + id.get(this) + " ";
            } else {
                requete += " WHERE " + identifiant + "= '" + id.get(this) + "'";
            }
            System.out.println(requete);
            Statement stmt = con.createStatement();
            stmt.execute(requete);
            if (secours) {
                con.commit();
            }

        } catch (Exception e) {
            con.rollback();
            System.out.println(e);
        } finally {
            if (secours) {
                con.close();
            }
        }
    }

    public Field idUpdate(String identifiant) throws Exception {
        Field[] att = this.getClass().getDeclaredFields();
        Field id = null;
        boolean ok = false;
        for (int i = 0; i < att.length; i++) {
            if (att[i].getName().equals(identifiant)) {
                ok = true;
                id = att[i];
            }
        }
        if (ok == false) {
            throw new Exception("Aucun attribut de la classe " + this.getTableName() + " n'a comme nom " + identifiant);
        }
        return id;
    }

    public boolean notNull(Field attField) throws IllegalArgumentException, IllegalAccessException {
        if (attField.getType().getSimpleName().equals("String")) {
            if (attField.get(this) == null) {
                return false;
            }
        }
        if (attField.getType().getSimpleName().equals("Integer")) {
            if (attField.get(this) == null) {
                return false;
            }
            Integer val;
            val = Integer.valueOf(String.valueOf(attField.get(this)));
            if (val == null && attField.getType().getSimpleName().equals("Integer")) {
                return false;
            }
        }
        if (attField.getType().getSimpleName().equals("Double")) {
            if (attField.get(this) == null) {
                return false;
            }
            Double val;
            val = Double.valueOf(String.valueOf(attField.get(this)));
            if (val == null) {
                return false;
            }
        }
        if (attField.getType().getSimpleName().equals("Boolean")) {
            if (attField.get(this) == null) {
                return false;
            }
            Boolean valBol = Boolean.valueOf(String.valueOf(attField.get(this)));
            if (valBol == false) {
                return false;
            }
        }
        if (attField.getType().getSimpleName().equals("Date")) {
            if (attField.get(this) == null) {
                return false;
            }
        }
        return true;
    }

    public boolean Allnull() throws IllegalArgumentException, IllegalAccessException {
        Field[] attribs = this.getClass().getDeclaredFields();
        boolean vide = true;
        for (int i = 0; i < attribs.length; i++) {
            attribs[i].setAccessible(true);
            if (notNull(attribs[i])) {
                vide = false;
            }
        }
        return vide;
    }

    public boolean AllnullUpdate(String attribut) throws IllegalArgumentException, IllegalAccessException {
        Field[] attribs = this.getClass().getDeclaredFields();
        boolean vide = true;
        for (int i = 0; i < attribs.length; i++) {
            attribs[i].setAccessible(true);
            if (attribs[i].getName().equals(attribut)) {
                continue;
            }
            if (notNull(attribs[i])) {
                vide = false;
            }
        }
        return vide;
    }

    public String transformTypeCol(Object type) {
        String trueType = type.toString();
        if (trueType.equals("int") || trueType.equals("double") || trueType.equals("Double")
                || trueType.equals("Integer")) {
            return "NUMERIC";
        }
        if (trueType.equals("String") || trueType.equals("boolean") || trueType.equals("Double")) {
            return "VARCHAR";
        }
        if (trueType.equalsIgnoreCase("Date")) {
            return "DATE";
        }
        return "VARCHAR";
    }

    public void dropTable(Connection connection) throws SQLException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        if (connection == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = connection;
        }
        try {
            requete += " DROP TABLE " + this.getTableName() + " ";
            System.out.println(requete);
            Statement stmt = con.createStatement();
            stmt.execute(requete);
            if (secours) {
                con.commit();
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (secours) {
                con.close();
            }
        }
    }

    public void createSequence(Connection connection) throws SQLException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        if (connection == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = connection;
        }
        try {
            requete += " CREATE SEQUENCE id" + this.getTableName() + " ";
            requete += " MINVALUE 1 MAXVALUE 99999999";
            requete += " START WITH 1 INCREMENT BY 1";
            // requete += " CACHE 20";
            System.out.println(requete);
            Statement stmt = con.createStatement();
            stmt.execute(requete);
            if (secours) {
                con.commit();
            }

        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            if (secours) {
                con.close();
            }
        }
    }

    public void insertX(Connection connection, int zero)
            throws SQLException, IllegalArgumentException, IllegalAccessException {
        Connection con = null;
        boolean secours = false;
        String requete = "";
        String idObject = "";
        if (connection == null) {
            secours = true;
            con = new Connect().getConnectionPostgresql();
        } else {
            con = connection;
        }
        try {
            Vector<Object> attributs = this.getAttributsValues();
            Vector<Object> attribsType = this.getAttributsType();
            Field[] fields = this.getClass().getDeclaredFields();
            boolean idSetted = false;
            requete += "INSERT INTO " + this.getTableName() + " VALUES (";
            for (int i = 0; i < attributs.size(); i++) {
                if (isAnArray(fields[i]) || attribsType.get(i).equals("Service")
                        || attribsType.get(i).equals("Candidat") || attribsType.get(i).equals("Specialite")
                        || attribsType.get(i).equals("Gain") || attribsType.get(i).equals("Finition")
                        || attribsType.get(i).equals("Format")) {
                    continue;
                }
                if (transformTypeCol(attribsType.get(i)).equals("NUMERIC")
                        || transformTypeCol(attribsType.get(i)).equals("DATE") && attributs.get(i) == null
                        || transformTypeCol(attribsType.get(i)).equals("VARCHAR") && attributs.get(i) == null) {
                    requete += attributs.get(i);
                } else {
                    if (fields[i].getName().toString().contains("id")
                            && fields[i].getName().toString().contains(this.getTableName()) && !idSetted) {
                        String ress = getSequenceValue(con);
                        idSetted = true;
                        int zeroNumber = zero - Integer.valueOf(ress.length());
                        requete += "'" + this.getTableName();
                        idObject += this.getTableName();
                        ;
                        for (int ii = 0; ii < zeroNumber; ii++) {
                            requete += "0";
                            idObject += "0";
                        }

                        requete += ress + "'";
                        idObject += ress;
                        fields[i].setAccessible(true);
                        fields[i].set(this, idObject);
                    } else {
                        requete += "'" + attributs.get(i) + "'";
                    }
                }
                if (i < attributs.size() - 1 && !isAnArray(fields[i + 1])) {
                    requete += ",";
                }
            }
            requete += ")";
            System.out.println(requete);
            // System.out.println(requete);
            Statement stmt = con.createStatement();
            stmt.execute(requete);
            if (secours) {
                con.commit();
            }
        } catch (SQLException e) {
            con.rollback();
            System.err.println(e);
        } finally {
            if (secours) {
                con.close();
            }
        }
    }

    public String getSequenceValue(Connection con) throws SQLException {
        String requeteId = "SELECT nextval('id" + this.getTableName() + "') ";

        Statement stmtId = con.createStatement();
        ResultSet resu = stmtId.executeQuery(requeteId);
        String ress = "";
        while (resu.next()) {
            ress = resu.getString(1);
        }

        return ress;
    }

    public boolean isAnArray(Field field) {
        boolean isArray = false;
        if (field.getType().isInstance(new Vector<>())) {
            isArray = true;
        } else if (field.getType().isInstance(new ArrayList<>())) {
            isArray = true;
        } else if (field.getType().isArray()) {
            isArray = true;
        } else if (field.getType().getSimpleName().equals("InterService")) {
            isArray = true;
        } else if (field.getType().getSimpleName().equals("SousService")) {
            isArray = true;
        } else if (field.getType().getSimpleName().equals("AchatService")) {
            isArray = true;
        } else if (field.getType().getSimpleName().equals("Client")) {
            isArray = true;
        } else if (field.getType().getSimpleName().equals("Remise")) {
            isArray = true;
        }

        return isArray;
    }

}