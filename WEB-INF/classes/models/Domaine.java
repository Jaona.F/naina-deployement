package models;

import java.sql.Connection;
import java.util.Vector;

import Annotation.Choosen;
import Annotation.JsonData;
import Annotation.Method;
import connection.Connect;
import table.Relation;
import utilities.ModelView;

@Choosen
public class Domaine extends Relation {

    String idDomaine = "";

    public Domaine() {
    }

    public Domaine(Vector<Object> datas) {
        this.idDomaine = (String) datas.get(0);
        this.name = (String) datas.get(1);
        this.counter = Integer.valueOf(String.valueOf(datas.get(2)));
    }

    @Method(urlTo = "get-Domaine-page.do")
    public ModelView getEmpById(int id, String test) {
        ModelView view = new ModelView();
        view.setUrl("Domaine.jsp");
        this.counter = this.counter + 1;
        view.addItem("counter", this.counter);
        return view;
    }

    @Method(urlTo = "insert-domaine.do")
    public ModelView insertDomaine(int isa) throws Exception {
        ModelView view = new ModelView();
        Domaine domaine = new Domaine();

        domaine.setCounter(isa);
        domaine.setName("popo Paul");
        try {
            Connect connect = new Connect();
            Connection con = connect.getConnectionPostgresql();
            domaine.insertX(con, 5);
            con.commit();
            con.close();
        } catch (Exception e) {
            throw new Exception(e);
        }

        view.setUrl("Domaine.jsp");
        return view;
    }

    @JsonData
    @Method(urlTo = "select-domaine.do")
    public Object select_domaine() throws Exception {

        Domaine domaine = new Domaine();
        Vector<Object> datas = new Vector<>();
        try {
            Connect connect = new Connect();
            Connection con = connect.getConnectionPostgresql();
            datas = domaine.select(con, true);
            con.close();
        } catch (Exception e) {
            throw new Exception(e);
        }

        return datas;
    }

    @Method(urlTo = "insert-domaine-page.do")
    public ModelView domaine_page() throws Exception {
        ModelView view = new ModelView();
        view.setUrl("Form.jsp");
        return view;
    }

    @JsonData
    @Method(urlTo = "insert_domain_form.do")
    public Object insert_domaine() throws Exception {
        ModelView view = new ModelView();
        view.setUrl("Form.jsp");
        this.setIdDomaine("");
        try {
            Connect connect = new Connect();
            Connection con = connect.getConnectionPostgresql();
            this.insertX(con, 5);
            con.commit();
            con.close();
        } catch (Exception e) {
            throw new Exception(e);
        }
        return this;
    }

    public String getName() {
        return name;
    }

    public int getPopu() {
        return popu;
    }

    public void setPopu(int popu) {
        this.popu = popu;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getIdDomaine() {
        return idDomaine;
    }

    public void setIdDomaine(String idDomaine) {
        this.idDomaine = idDomaine;
    }

}
