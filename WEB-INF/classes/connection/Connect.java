package connection;

import java.sql.*;

public class Connect {

    public Connect() {

    }

    public Connection getConnectionPostgresql() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");

            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/naina", "postgres", "popo");
            con.setAutoCommit(false);
        } catch (Exception e) {
            System.out.println(e);

        }
        return con;
    }

    public Connection getConnectionOracle() {
        Connection con = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/XE", "Paul", "popo");
            con.setAutoCommit(false);

        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }

}
