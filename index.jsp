<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/deployement/assets/css/font.css" />
    <link rel="stylesheet" href="/deployement/assets/css/index.css" />
    <title>Acceuil</title>
  </head>
  <body>
    <div id="root">
      <div class="middle-section">
        <div class="title-section">Vous voulez faire quoi ?</div>
        <div class="list-response">
          <div class="response-box">
            <div class="text-response">Voir liste des sessions</div>
            <div class="detail-response" data-url="checkSession.do"></div>
          </div>
          <div class="response-box">
            <div class="text-response">Se connecter ( admin )</div>
            <div class="detail-response" data-url="login.do"></div>
          </div>
          <div class="response-box">
            <div class="text-response">Se déconnecter</div>
            <div class="detail-response" data-url="logout.do"></div>
          </div>
          <div class="response-box">
            <div class="text-response">Voir page upload</div>
            <div class="detail-response" data-url="UploadPage.do"></div>
          </div>

          <div class="response-box">
            <div class="text-response">View datas en Json</div>
            <div class="detail-response" data-url="view-to-json.do"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- <form method="post" action="upload.do" enctype="multipart/form-data">
      Choose a file: <input type="file" name="empPicture" />
      <input type="submit" value="Upload" />
    </form> -->
    <script type="module" src="/deployement/assets/js/index.js"></script>
  </body>
</html>
