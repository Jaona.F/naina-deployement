<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Domaine</title>
    <link rel="stylesheet" href="/deployement/assets/css/font.css" />
    <link rel="stylesheet" href="/deployement/assets/css/index.css" />
    <link rel="stylesheet" href="/deployement/assets/css/Form.css" />

    <link rel="stylesheet" href="/deployement/assets/css/upload.css" />
  </head>
  <body>
    <div id="root">
      <div class="middle-section">
        <div class="title-section">Insert Domaine</div>
        <form
          method="post"
          action="insert_domain_form.do"
          class="form-upload"
          id="form_domaine"
          enctype="multipart/form-data"
        >
          <div class="row-form">
            <div class="label-form">Nom :</div>
            <input type="text" placeholder="XXXX" name="name" />
          </div>
          <div class="row-form">
            <div class="label-form">Counter :</div>
            <input type="number" placeholder="000" name="counter" />
          </div>
          <div class="row-form">
            <div class="label-form">Population :</div>
            <input type="number" placeholder="000" name="popu" />
          </div>
          <div class="row-form">
            <button class="btn_validate">Valider</button>
          </div>
        </form>
      </div>
    </div>
    <script src="/deployement/assets/js/Form.js"></script>
  </body>
</html>
